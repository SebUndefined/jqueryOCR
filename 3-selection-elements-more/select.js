

$(function() {
  //here, the DOM is defined (shorter version)
  //picture which have the attribute (partially or totally) border to 1
  $('[border*="1"]').css('border-color', 'red');
  //Picture which have the title attribute with "animal" delimited by a space
  $('[title~="animal"]').css('border-color', 'blue');
  // image where src attribute end by e.jpg
  $('[src$="e.jpg"]').css('border-color', 'pink');
  //Image where border is different of 15
  $('[border!="15"]').css("border-color","black");
  //Image where src start by ch
  $('[src^="ch"]').css("border-color","green");

  //uncomment the desire directive

  //direct child ul element of li 2, 3
  //$('li > ul').css('color','red');
  //Elements li directly preceded by an element li 3, 4, 5, 6
  //$('li + li').css('color','red');
  //First child li element  1, 2, 3
  //$('li:first-child').css('color','red');
  //First li element  1, 2, 3
  //$('li:first').css('color','red');
  //Last li element 6
  //$('li:last').css('color','red');
  //Last li child element 3, 6
  //$('li:last-child').css('color','red');
  //unique child of their parent none
  //$('li:only-child').css('color','red');
  //second child of li 3,4
  $('li:nth-child(2)').css('color','red');

  //Paragraphes pair 1, 3, 5
  $('p:even').css('color', 'green');
  //Paragraphes impair 2,4
  $('p:odd').css('color', 'pink');
  //Element p après le deuxième 3, 4, 5
  $('p:gt(1)').css('color', 'yellow');
  //p index 4
  $('p:eq(3)').css('color', 'black');
  //p avant 4 1,2,3
  $('p:lt(3)').css('color', '#320933')

  //Title
  $('#cache').hide();
  //Tous les titre en rouge
  $(':header').css('color','red');
  //Montrer le div #cache
  $('#cache:hidden').show();
  //Cacher tous les titres sauf h1
  $(':header:not(h1)').hide();
});
