

$(function() {
    //Coloration de la première cellule
    // $('td:first').css('color','red');   
    // //Coloration de la dernière ligne
    // $('tr:last').css('color','red'); 
    // //Coloration des cellules vides
    // $(':empty').css('color','red');
    // // Coloration des lignes paires
    // $('tr:even').css('color','red');
    // //Coloration des cellules paires
    // $('td:even').css('color','red');
    // //Coloration des lignes d'index supérieur à 2
    // $('tr:gt(1)').css('color','red');
    //Coloration des lignes d'index supérieur à 1, et, 
    //dans le sous-ensemble correspondant, des lignes 
    //d'index inférieur à 5
    // $('tr:gt(0):lt(4)').css('color','red');
    //Coloration des cellules d'index supérieur à 5, 
    //et, dans le sous-ensemble correspondant, 
    //des cellules d'index inférieur à 11
    //$('td:gt(4):lt(10)').css('color', 'green');

    //Coloration de toutes les lignes à l'exception 
    //de la dernière
    $('tr:not(tr:last)').css('color','pink');
});
