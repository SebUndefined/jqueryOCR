

$(function() {
    //Coloration de tous les champs de saisie
    $(':input').css('background','yellow');
    //Coloration d'un champ de saisie particulier
    $(':password').css('background','yellow');
    //Redimensionnement d'un champ de type image
    $(':image').css('width','100px');
    // Focus au premier champ de saisie et coloration en jaune
    document.forms[0].nom.focus(); $(':focus').css('background','yellow');
});
