$(function() {
  $('#couleur-fond').change(function() {
        $('#contenu').css('background-color', $(this).val());
    });
  $('#texte').change(function() {
    switch($(this).val()) {
    case "Normal":
        $('#contenu p').css('font-weight', 'normal');
        $('#contenu p').css('font-style', 'normal');
        $('#contenu p').css('text-decoration', 'none');
        break;
    case "Gras":
        $('#contenu p').css('font-weight', 'bold');
        break;
    case "Italique":
        $('#contenu p').css('font-style', 'italic');
        break;
    case "Souligne":
        $('#contenu p').css('text-decoration', 'underline');
        break;
    default:
        $('#contenu p').css('font-weight', 'normal');
        $('#contenu p').css('font-style', 'normal');
        $('#contenu p').css('text-decoration', 'none');
    } 
  });
  $('#police').change(function() {
        $('#contenu p').css('font-family', $(this).val());
    });
  $('#police-prem-phrase').change(function() {
        $('#contenu p:first').css('font-family', $(this).val());
    });
  $('#prem-car-phrases').change(function() {
    var valueSelected = $(this).val();
    if (valueSelected == "Gras") {
        $('p').each(function() {
            var resTab = $(this).text().split('. ');
            if (resTab.length == 0) {
                
            } else {
                var tab2 = $.map(resTab, function(el, ind) {
                    if (el[0] != null) {
                        return '<b>' + (el[0]) + '</b>' + el.substring(1) + '. ';
                    }
                });
                $(this).html(tab2.join(''));
            }
            
        });
    }
    else if (valueSelected == "Gras") {
        
    }
     if (valueSelected == 'Normal') {
        $('p').each(function() {
        var unPar = $(this).html();
        if (unPar.indexOf('<img') == -1)
            $(this).text($(this).text());
        });
     }
    });

    $('#couleurMot').click(function() { 
        var wordNumber = parseInt($('#mot').val());
        var tabFirstParaf = $('p:first').text().split(' ');
        tabFirstParaf[wordNumber+1] = '<font color="red">' + tabFirstParaf[wordNumber+1] + '</font>'
        $('p:first').html(tabFirstParaf.join(' '));
    });
    $('#bordure-images').change(function() { 
        var borderSelected = $(this).val();
        switch(borderSelected) {
            case "Rien":
                $('#image').css('border', 'none');
                break;
            case "Simple":
                $('#image').css('border', '2px solid red');
                break;
            case "Double":
                $('#image').css('border', '2px double red');
                break;
            default:
                alert('Erreur');
        } 
    });
    $('#raz').click(function() { 
        location.reload();
    });

});
