$(function() {
    $('#action').click(function() {
      var leTexte = $('#texte').val();
      var laPosition = $('#position').val();
      $('#resultat').html('Texte original           : "' + leTexte + '"' + 
                      '<br>Après la fonction trim() : "' + $.trim(leTexte) + '"' +
                      '<br>Le caractère en position ' + laPosition + ' est un : "' + leTexte.charAt(laPosition) + '"');
    });  
    $('#changement').click(function() {
      $('#un').replaceWith('<img id="unbis" src="mauvais.png">');
    }); 
    $('#interdit').mouseover(function() {
      $(this).fadeOut(1000);
      $('#seb').fadeIn(1000);
    });
    $('#seb').mouseout(function() {
      $(this).fadeOut(1000);
      $('#interdit').fadeIn(1000);
    });
});
