

$(function() {
    $('#julia').addClass('rouge');
    $('#julia').addClass('vert grand');
    $('#eric').removeClass('vert').addClass('rouge');
    $('#paul').removeClass('vert').addClass('rouge').removeClass('grand').addClass('petit');
    $('#paul').toggleClass('vert').toggleClass('rouge').toggleClass('grand').toggleClass('petit');
    $('#pierre').removeClass('vert grand').addClass('rouge petit');
    if ($('#jean').hasClass('rouge'))
        alert('le span #jean est de classe rouge');
    else
        alert('le span #jean n\'est pas de classe rouge');
    if ($('#jean').is('.grand.rouge'))
        alert('le span #jean est de classe grand et rouge');
    else
        alert('le span #jean n\'est pas de classe grand et/ou rouge');
});
