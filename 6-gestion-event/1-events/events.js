$(function() {
    $('#target').css('width','50px');
     // Dimensions de la fenêtre
    var largeur = ($(window).width()) - 50;
    var hauteur = ($(window).height()) - 50;

    // Affichage de la première image en (100, 100)
    var p = $('#target').offset();
    p.top=100;
    p.left=100;
    $('#target').offset(p);
    
    // Gestion du clic et déplacement de l'image
    // $("#target").click(function()
    $("#target").mouseenter(function(){
      x = Math.floor(Math.random()*largeur);
      y = Math.floor(Math.random()*hauteur);
      var p = $('#target').offset();
      p.top = y;
      p.left = x;
      $('#target').offset(p);
    });
});
