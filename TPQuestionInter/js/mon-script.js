$(function() {
    $('.reponse').hide();
    var quest = $('.question');
    quest.css('background','#9EEAE0');
    quest.css('border-style', 'groove');
    quest.css('border-width', '4px');
    quest.css('width', '900px');
    quest.css('height', '250px');
    quest.css('margin', '20px');
    $('.texte').css('float', 'left');
    $('.texte').css('width', '90%');
    $('img').css('float', 'right');
    $('img').css('margin-top', '80px');
    
    
    $('a').hover(
        function(){
            $('.reponse').show();
            if($(':radio[id="r1"]:checked').val()){
                $('#reponse1').css('color', 'green');
                $('#img1').attr('src',"./images/bon.png");
            }
            else {
                $('#reponse1').css('color', 'red');
                $('#img1').attr('src',"./images/mauvais.png");
            }
            if($(':radio[id="r4"]:checked').val()){
                $('#reponse2').css('color', 'green');
                $('#img2').attr('src',"./images/bon.png");
            }
            else {
                $('#reponse2').css('color', 'red');
                $('#img2').attr('src',"./images/mauvais.png");
            }
            if($(':radio[id="r8"]:checked').val()){
                $('#reponse3').css('color', 'green');
                $('#img3').attr('src',"./images/bon.png");
            }
            else {
                $('#reponse3').css('color', 'red');
                $('#img3').attr('src',"./images/mauvais.png");
            }
        },
        function(){
            $('.reponse').hide();
            $('img').attr('src',"./images/question.png");
        }
    );
});