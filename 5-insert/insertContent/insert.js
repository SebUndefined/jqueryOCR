

$(function() {
    $('h2').append(' ***');
    $('h2').prepend('*** ');
    //$('#trois').before('<hr>');
    //$('hr').after('<br><br>');
    //$('hr').replaceWith('<br>');
    // $('h2').each(function(){
    //     var elemH2 = $(this);
    //     elemH2.replaceWith('<h3>' + elemH2.text() + '</h3>');
    // });
    $('<li>Deuxième élément bis</li>').insertAfter($('li:nth-child(2)'));
    $('<hr>').prependTo($('h2'));
    //déplace p1 après p2
    $('#p2').after($('#p1'));
    //Clone le p1 et le met après h2 un
    $('#p1').clone().insertAfter('#un');
    $('ul').clone().insertBefore($('h2:first'));
    //Création d'un sommaire
    $('<h1>Sommaire</h1>').insertBefore($('h2:first'));
    $('h2').each(function(){
        $('<a href="#'+ this.id + '">' + this.innerHTML + '</a>').insertAfter('h1');
    });
});
