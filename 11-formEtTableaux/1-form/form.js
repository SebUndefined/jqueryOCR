$(function() {
  $('#user').focus();
  var leFocus;
  var idFocus;
  var outFocus;
  $('input, textarea').focus(function(){
     leFocus = $(this).attr('id');
     $('#status').text(leFocus + ' a le focus');
     idFocus = '#' + leFocus;
     $(idFocus).css('background-color', '#afc');
  });
  $('input, textarea').blur(function(){
     outFocus = '#' + $(this).attr('id');
     $(outFocus).css('background-color', '#fff');
  });

});
