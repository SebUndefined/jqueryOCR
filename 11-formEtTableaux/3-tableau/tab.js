$(function() {
  var tableau = ['Luca', 'Emma', 'Mathis', 'Jade', 'Léa', 'Enzo', 'Chloé'];
	var tableau2;
	$('#un').text('Données originales : ' + tableau.join(', '));
	$('#copie1').click(function() {
	  tableau2 = $.map(tableau, function(el,ind) {
		return (el.toUpperCase());
	  });
	  $('#deux').text('Prénoms en majuscules : ' + tableau2.join(', '));
	});
	$('#copie2').click(function() {
	  tableau2 = $.map(tableau, function(el,ind) {
		return (ind + ' : ' + el.toLowerCase());
	  });
	  $('#deux').text('Index et prénoms en minuscules : ' + tableau2.join(', '));
	});
});
