$(function() {
    $('tr:even').css('background','yellow');
    $('td').css('width','200px');
    $('td').css('text-align','center');
    //Redéfinition de la valeur de slow
    jQuery.fx.speeds.slow = 1500;
    //Ajout d'une variable super-slow
    jQuery.fx.speeds['super-slow'] = 3000;

    $('#affiche').click(function() {
      $('tr:even').show('slow', 'linear');
    } ); 
    $('#cache').click(function() {
      //$('tr:even').hide(1000);
      $('tr:even').hide('super-slow');
    });
});
