$(function() {
    //Si click sur les bouton portant classe before
    $('.before').click(function(){
        //On supprime le formulaire si présent
        $('#before').remove();
        //On supprimer la hr si présent
        $('DIV#droite hr').remove();
        //On ajoute un hr
        $('<hr>').appendTo('#droite');
        //On créé un form
        $('<form></form>').attr('id','before').appendTo('#droite');
        //En fonction de l'id du boutton, on créer le span
        switch(this.id) {
        case "text":
            monType = "text";
            createForm("ID de la zone de texte: ", this.id);
            break;
        case "label":
            createForm("Texte du label: ", this.id);
            break;
        case "button":
            createForm("Texte du boutton: ", this.id);
            break;
        default:
            alert('Erreur système');
        }   				
    });

    
}); 

function createForm(label, type) {
    //On ajoute un label au formulaire
    $('<span>'+ label +'</span>').appendTo('#before');
    //On ajoute un input au formulaire
    $('<input>').appendTo('#before');
    //On lui attribut un ID
    var id = 'data-' + type;
    $('#before input').attr('id', id);
    //On indique que le champ est requis
    $('#before input').prop('required', true);
    //On ajoute un bouton
    $('<button>OK</button>').appendTo('#before');
    $('#before button').attr('id', 'submit');
    //On définit la méthode à executer et on annule le comportement par défaut
    $('#before').submit(function(event) {
        createElement($('#' + id).val(), type);
        event.preventDefault();
        });      
    }

function createElement(value, type) {
    //Si l'élément de gauche n'a pas d'enfant, on créé le formulaire
        if($('#gauche > *').children().length == 0) {
            $('<form></form>').appendTo('#gauche');
            $('#gauche form').attr('id', 'my-form');
            $('#my-form').css('padding', '10px');
        }
        //En fonction du type demandé, on créé le controle. 
       switch(type) {
        case "text":
            $('<input>').appendTo('#my-form');
            $('#my-form input').attr('id', value).css('margin-left', '20px');
            $('<br>').appendTo('#my-form');
            break;
        case "label":
            $('<span>'+ value +': </span>').appendTo('#my-form');
            break;
        case "button":
            $('<button>'+ value +'</button>').appendTo('#my-form');
            break;
        default:
            alert('Erreur système');
        }
        //On supprime le formulaire de création
        $('#before').remove();  
    }