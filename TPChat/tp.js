 $(function() {
   refresh();
     $('#envoyer').click(function(){
       var nom = $.trim($('#nom').val());
       var message = $('#message').val();
       if(nom.length > 0 && message.length > 0) {
         $.ajax({
            type: "POST",
            url: 'chat.php',
            data: {'nom': nom, 'message': message},
            complete: refresh,
            dataType: "json"
          });
       }
       else {
         alert('Formulaire invalide...');
       }
     });
     function refresh() {
        $('#conversation').load('ac.htm');
        $('#message').val('');
        $('#message').focus();
       }
       setInterval(refresh, 4000);
    });
