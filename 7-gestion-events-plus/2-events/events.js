$(function() {
    // $('img').one('click', function() {
    //   $('#message').text('Vous avez cliqué sur l\'image. Désormais, je resterai insensible aux clics.').fadeIn(1000).fadeOut(5000);
    // });
    $('img').click(function(event,texte) {
      if (texte == undefined)
        texte = "par vous";
      $('#message').text('L\'image a été cliquée ' + texte).fadeIn(1000).fadeOut(1000);
    });
    $('button').click(function() {
      $('img').trigger('click', 'par jQuery');
    });
});
